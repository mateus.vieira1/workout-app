class JsonWebToken
  class ExpiredToken < StandardError; end
  class InvalidSignature < StandardError; end

  SECRET = ENV['JWT_SECRET']
  def self.encode(payload, duration = 30.seconds)
    payload[:exp] = duration.from_now.to_i
    JWT.encode payload, SECRET, 'HS256'
  end

  def self.decode(token)
    HashWithIndifferentAccess.new JWT.decode(token, SECRET).first
  rescue JWT::VerificationError
    raise JsonWebToken::InvalidSignature
  rescue JWT::ExpiredSignature
    raise JsonWebToken::ExpiredToken
  end
end
