class ExercisesController < ApplicationController
  before_action :set_exercise, only: %i[show edit update destroy]
  before_action :authenticate_user!
  before_action :verify_token

  # GET /exercises
  def index
    @exercises = Exercise.all
  end

  # GET /exercises/1
  def show; end

  # GET /exercises/new
  def new
    @exercise = Exercise.new
  end

  # GET /exercises/1/edit
  def edit; end

  # POST /exercises
  def create
    @exercise = Exercise.new(exercise_params)

    if @exercise.save
      redirect_to exercise_url(@exercise), notice: 'Exercise was successfully created.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /exercises/1
  def update
    if @exercise.update(exercise_params)
      redirect_to exercise_url(@exercise), notice: 'Exercise was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /exercises/1
  def destroy
    @exercise.destroy

    redirect_to exercises_url, notice: 'Exercise was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_exercise
    id = params[:id]
    @exercise = Exercise.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    redirect_to exercises_path, alert: "Exercise with id '#{id}' not found!"
  end

  # Only allow a list of trusted parameters through.
  def exercise_params
    params.require(:exercise).permit(:description, :intensity)
  end
end
