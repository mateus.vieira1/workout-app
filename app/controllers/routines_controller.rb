class RoutinesController < ApplicationController
  before_action :set_routine, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: %i[index show]
  before_action :verify_token, except: %i[index show]

  # GET /routines
  def index
    @routines = Routine.all
  end

  # GET /routines/1
  def show; end

  # GET /routines/new
  def new
    @routine = Routine.new
  end

  # GET /routines/1/edit
  def edit; end

  # POST /routines
  def create
    @routine = Routine.new(routine_params)
    if @routine.save
      redirect_to routine_url(@routine), notice: 'Routine was successfully created.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /routines/1
  def update
    if @routine.update(routine_params)
      redirect_to routine_url(@routine), notice: 'Routine was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /routines/1
  def destroy
    @routine.destroy
    redirect_to routines_url, notice: 'Routine was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_routine
    id = params[:id]
    @routine = Routine.find(id)
  rescue ActiveRecord::RecordNotFound
    redirect_to routines_path, alert: "Routine with id '#{id}' not found!"
  end

  # Only allow a list of trusted parameters through.
  def routine_params
    params.require(:routine).permit(:name, exercise_ids: [])
  end
end
