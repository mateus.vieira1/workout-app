# frozen_string_literal: true

class ApplicationController < ActionController::Base
  def verify_token
    JsonWebToken.decode(current_user.jwt_key)
    current_user.update_token!
  rescue JsonWebToken::ExpiredToken
    sign_out
    redirect_to new_user_session_path, alert: 'Token expired!'
  end
end
