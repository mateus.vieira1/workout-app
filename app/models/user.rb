class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # , :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable, :lockable

  def update_token!
    payload = { id:, timestamp: Time.now }
    update(jwt_key: JsonWebToken.encode(payload))
  end
end
