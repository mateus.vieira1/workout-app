class Exercise < ApplicationRecord
  has_and_belongs_to_many :routines

  validates :description, presence: true
  validates :intensity, presence: true,
                        numericality: { only_integer: true, in: 0..10 }

  def description_with_intensity
    "#{description} - #{intensity}"
  end
end
