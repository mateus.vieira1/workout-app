FactoryBot.define do
  factory :exercise do
    description { Faker::Lorem.paragraph }
    intensity { Faker::Number.between(from: 0, to: 10) }

    trait :invalid_desc do
      description { '' }
    end
  end
end
