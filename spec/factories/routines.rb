FactoryBot.define do
  factory :routine do
    name { Faker::Name.name }
  end
end
