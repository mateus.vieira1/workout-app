RSpec.shared_examples 'prevent route access without authentication' do
  it 'redirects to sign in page' do
    subject
    expect(response).to redirect_to(new_user_session_path)
  end
end
