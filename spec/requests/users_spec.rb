require 'rails_helper'

RSpec.describe '/users', type: :request do
  let(:user) { create(:user) }
  describe 'POST /sign_in' do
    subject do
      post user_session_path, params: { user: { email: user.email, password: '123456' } }
    end
    it 'updates user token' do
      expect { subject }.to change { user.reload.jwt_key }
    end
  end
end
