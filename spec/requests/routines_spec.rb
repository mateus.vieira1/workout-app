require 'rails_helper'

RSpec.describe '/routines', type: :request do
  let(:valid_attributes) do
    { name: 'New routine' }
  end

  let!(:valid_routine) { create(:routine) }

  describe 'with authenticated user' do
    let(:user) { create(:user) }
    before :each do
      sign_in user
    end

    describe 'GET /new' do
      it 'renders a successful response' do
        get new_routine_url
        expect(response).to be_successful
      end
    end

    describe 'GET /edit' do
      it 'renders a successful response' do
        get edit_routine_url(valid_routine)
        expect(response).to be_successful
      end
    end

    describe 'POST /create' do
      it 'creates a new Routine' do
        expect do
          post routines_url, params: { routine: valid_attributes }
        end.to change(Routine, :count).by(1)
      end

      it 'redirects to the created routine' do
        post routines_url, params: { routine: valid_attributes }
        expect(response).to redirect_to(routine_url(Routine.last))
      end
    end

    describe 'PATCH /update' do
      let(:new_attributes) do
        { name: 'new name' }
      end

      subject { patch routine_url(valid_routine), params: { routine: new_attributes } }

      it { expect { subject }.to change { valid_routine.reload.name } }

      it 'redirects to the routine' do
        expect(subject).to redirect_to(routine_url(valid_routine))
      end
    end

    describe 'DELETE /destroy' do
      subject { delete routine_url(valid_routine) }
      it 'destroys the requested routine' do
        expect { subject }.to change(Routine, :count).by(-1)
      end

      it 'redirects to the routines list' do
        subject
        expect(response).to redirect_to(routines_url)
      end
    end

    describe 'with invalid id' do
      let!(:invalid_routine_id) { 115 }

      describe 'GET /show' do
        it 'redirects to /index' do
          get routine_url(invalid_routine_id)
          expect(response).to redirect_to(routines_url)
        end
      end
      describe 'GET /edit' do
        it 'redirects to /index' do
          get edit_routine_url(invalid_routine_id)
          expect(response).to redirect_to(routines_url)
        end
      end
      describe 'PATCH /update' do
        it 'redirects to /index' do
          patch routine_url(invalid_routine_id)
          expect(response).to redirect_to(routines_url)
        end
      end
      describe 'DELETE /destroy' do
        it 'redirects to /index' do
          delete routine_url(invalid_routine_id)
          expect(response).to redirect_to(routines_url)
        end
      end
    end
  end

  describe 'without authenticated user' do
    describe 'GET /index' do
      it 'renders a successful response' do
        get routines_url
        expect(response).to be_successful
      end
    end
    describe 'GET /show' do
      it 'renders a successful response' do
        get routine_url(valid_routine)
        expect(response).to be_successful
      end
    end

    describe 'GET /new' do
      subject { get new_routine_url }
      it_behaves_like 'prevent route access without authentication'
    end

    describe 'GET /edit' do
      subject { get edit_routine_url(valid_routine) }
      it_behaves_like 'prevent route access without authentication'
    end

    describe 'POST /create' do
      subject { post routines_url, params: { routine: valid_attributes } }
      it_behaves_like 'prevent route access without authentication'
    end

    describe 'PATCH /update' do
      let(:new_attributes) do
        { name: 'new name' }
      end
      subject { patch routine_url(valid_routine), params: { routine: new_attributes } }
      it_behaves_like 'prevent route access without authentication'
    end

    describe 'DELETE /destroy' do
      subject { delete routine_url(valid_routine) }
      it_behaves_like 'prevent route access without authentication'
    end
  end
end
