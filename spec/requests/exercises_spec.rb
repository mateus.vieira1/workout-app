require 'rails_helper'

RSpec.describe '/exercises', type: :request do
  let(:valid_attributes) do
    { description: 'test', intensity: 5 }
  end

  let(:invalid_attributes) do
    { description: '', intensity: 5 }
  end

  let!(:valid_exercise) { create(:exercise, intensity: 2) }
  describe 'with authenticated user' do
    let(:user) { create(:user) }
    before :each do
      sign_in user
    end

    describe 'GET /index' do
      it 'renders a successful response' do
        get exercises_url
        expect(response).to be_successful
      end
    end

    describe 'POST /create' do
      context 'with valid parameters' do
        it 'creates a new Exercise' do
          expect do
            post exercises_url, params: { exercise: valid_attributes }
          end.to change(Exercise, :count).by(1)
        end

        it 'redirects to the created exercise' do
          post exercises_url, params: { exercise: valid_attributes }
          expect(response).to redirect_to(exercise_url(Exercise.last))
        end
      end

      context 'with invalid parameters' do
        it 'does not create a new Exercise' do
          expect do
            post exercises_url, params: { exercise: invalid_attributes }
          end.to change(Exercise, :count).by(0)
        end

        it "renders 'new' template with unprocessable entity status" do
          post exercises_url, params: { exercise: invalid_attributes }
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end

    describe 'PATCH /update' do
      context 'with valid parameters' do
        let(:new_attributes) do
          { description: 'Changed', intensity: 8 }
        end

        subject { patch exercise_url(valid_exercise), params: { exercise: new_attributes } }
        it { expect { subject }.to change { valid_exercise.reload.description } }
        it { expect { subject }.to change { valid_exercise.reload.intensity } }

        it 'redirects to the exercise' do
          patch exercise_url(valid_exercise), params: { exercise: new_attributes }
          expect(response).to redirect_to(exercise_url(valid_exercise))
        end
      end

      context 'with invalid parameters' do
        it "renders 'edit' template with unprocessable entity status" do
          patch exercise_url(valid_exercise), params: { exercise: invalid_attributes }
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end

    describe 'DELETE /destroy' do
      subject do
        delete exercise_url(valid_exercise)
      end
      it 'destroys the requested exercise' do
        expect { subject }.to change(Exercise, :count).by(-1)
      end

      it 'redirects to the exercises list' do
        subject
        expect(response).to redirect_to(exercises_url)
      end
    end
    describe 'with invalid id' do
      let!(:invalid_exercise_id) { 115 }

      describe 'GET /show' do
        it 'redirects to /index' do
          get exercise_url(invalid_exercise_id)
          expect(response).to redirect_to(exercises_url)
        end
      end
      describe 'GET /edit' do
        it 'redirects to /index' do
          get edit_exercise_url(invalid_exercise_id)
          expect(response).to redirect_to(exercises_url)
        end
      end
      describe 'PATCH /update' do
        it 'redirects to /index' do
          patch exercise_url(invalid_exercise_id)
          expect(response).to redirect_to(exercises_url)
        end
      end
      describe 'DELETE /destroy' do
        it 'redirects to /index' do
          delete exercise_url(invalid_exercise_id)
          expect(response).to redirect_to(exercises_url)
        end
      end
    end
  end

  describe 'without authenticated user' do
    describe 'GET /index' do
      subject { get exercises_url }
      it_behaves_like 'prevent route access without authentication'
    end

    describe 'GET /show' do
      subject { get exercise_url(valid_exercise) }
      it_behaves_like 'prevent route access without authentication'
    end

    describe 'GET /new' do
      subject { get new_exercise_url }
      it_behaves_like 'prevent route access without authentication'
    end

    describe 'GET /edit' do
      subject { get edit_exercise_url(valid_exercise) }
      it_behaves_like 'prevent route access without authentication'
    end

    describe 'POST /create' do
      context 'with valid parameters' do
        subject { post exercises_url, params: { exercise: valid_attributes } }
        it_behaves_like 'prevent route access without authentication'
      end
    end

    describe 'PATCH /update' do
      context 'with valid parameters' do
        let(:new_attributes) do
          { description: 'Changed', intensity: 8 }
        end
        subject { patch exercise_url(valid_exercise), params: { exercise: new_attributes } }
        it_behaves_like 'prevent route access without authentication'
      end
    end

    describe 'DELETE /destroy' do
      subject { delete exercise_url(valid_exercise) }
      it_behaves_like 'prevent route access without authentication'
    end
  end
end
