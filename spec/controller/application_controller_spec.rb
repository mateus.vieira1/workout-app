require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  let!(:user) { create(:user) }

  before do
    allow(controller).to receive(:current_user) { user }
    allow(controller).to receive(:sign_out) { true }
  end
  controller do
    before_action :verify_token

    def index
      render plain: 'Custom called'
    end
  end

  describe 'with valid token' do
    it 'returns success' do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'with expired token' do
    it 'redirects to sign in page' do
      travel 1.minute
      get :index
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
