require 'rails_helper'

RSpec.describe Routine, type: :model do
  describe 'validations' do
    it { should have_and_belong_to_many(:exercises) }
  end

  describe 'with 1 exercise' do
    subject do
      exercise = build(:exercise)

      routine_1 = build(:routine)
      routine_1.exercises << exercise

      routine_2 = build(:routine)
      routine_2.exercises << exercise

      routine_1.save
      routine_2.save
    end

    it 'creates 2 routines' do
      expect { subject }.to change(Routine, :count).by(2)
    end
  end
end
