require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'with valid user' do
    let!(:user) { create(:user) }

    it 'updates to a valid token' do
      freeze_time do
        travel 1.minute
        expect { user.update_token! }.to change { user.reload.jwt_key }
        expect(JsonWebToken.decode(user.jwt_key)[:exp]).to eq(30.seconds.from_now.to_i)
      end
    end
  end
end
