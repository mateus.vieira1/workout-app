require 'rails_helper'

RSpec.describe Exercise, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :description }
    it { is_expected.to validate_presence_of :intensity }
    it { should have_and_belong_to_many(:routines) }
    it { should validate_numericality_of(:intensity).only_integer }

    let(:exercise) { build(:exercise) }
    it 'raises error for intensities out of range 0..10' do
      exercise.intensity = -1
      expect { exercise.save! }.to raise_error(ActiveRecord::RecordInvalid)
      exercise.intensity = 11
      expect { exercise.save! }.to raise_error(ActiveRecord::RecordInvalid)
    end
    it 'saves for intensity in range' do
      exercise.intensity = 10
      expect(exercise.save).to eq(true)
    end
  end

  describe 'with 1 routine' do
    subject do
      routine = build(:routine)

      exe_1 = build(:exercise)
      exe_1.routines << routine

      exe_2 = build(:exercise)
      exe_2.routines << routine

      exe_1.save
      exe_2.save
    end
    it 'creates 2 exercises' do
      expect { subject }.to change(Exercise, :count).by(2)
    end
  end
end
