require 'rails_helper'

RSpec.describe JsonWebToken do
  subject { JsonWebToken }
  let(:sign_date_time) { DateTime.new(2022, 1, 15) }
  let(:payload) { { id: 'test' } }
  it 'generates token with default duration' do
    freeze_time do
      token = subject.encode(payload)
      expect(subject.decode(token)[:exp]).to eq(30.seconds.from_now.to_i)
    end
  end
  it 'generates token with custom duration' do
    freeze_time do
      token = subject.encode(payload, 1.minute)
      expect(subject.decode(token)[:exp]).to eq(1.minute.from_now.to_i)
    end
  end
  it 'validates expiration time' do
    token = subject.encode(payload, 1.minute)
    travel 1.minute
    expect { subject.decode(token) }.to raise_error(JsonWebToken::ExpiredToken)
  end
end
