# frozen_string_literal: true

Rails.application.routes.draw do
  root 'routines#index'
  resources :exercises
  resources :routines
  devise_for :users, controllers: { sessions: 'users/sessions' }
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
