class CreateJoinTableExerciseRoutine < ActiveRecord::Migration[7.0]
  def change
    create_join_table :exercises, :routines 
  end
end
